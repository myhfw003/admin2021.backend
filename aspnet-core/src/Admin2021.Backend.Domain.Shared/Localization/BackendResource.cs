﻿using Volo.Abp.Localization;

namespace Admin2021.Backend.Localization
{
    [LocalizationResourceName("Backend")]
    public class BackendResource
    {

    }
}