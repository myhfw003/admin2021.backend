﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace Admin2021.Backend.EntityFrameworkCore
{
    public static class BackendDbContextModelCreatingExtensions
    {
        public static void ConfigureBackend(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(BackendConsts.DbTablePrefix + "YourEntities", BackendConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}