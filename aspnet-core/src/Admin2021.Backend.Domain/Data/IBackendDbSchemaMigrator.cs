﻿using System.Threading.Tasks;

namespace Admin2021.Backend.Data
{
    public interface IBackendDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
