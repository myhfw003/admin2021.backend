﻿using System;

namespace Admin2021.Backend.Models.Test
{
    public class TestModel
    {
        public string Name { get; set; }

        public DateTime BirthDate { get; set; }
    }
}