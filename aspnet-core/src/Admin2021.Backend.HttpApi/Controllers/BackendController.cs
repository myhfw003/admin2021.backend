﻿using Admin2021.Backend.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace Admin2021.Backend.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class BackendController : AbpController
    {
        protected BackendController()
        {
            LocalizationResource = typeof(BackendResource);
        }
    }
}