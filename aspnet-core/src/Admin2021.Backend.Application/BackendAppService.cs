﻿using System;
using System.Collections.Generic;
using System.Text;
using Admin2021.Backend.Localization;
using Volo.Abp.Application.Services;

namespace Admin2021.Backend
{
    /* Inherit your application services from this class.
     */
    public abstract class BackendAppService : ApplicationService
    {
        protected BackendAppService()
        {
            LocalizationResource = typeof(BackendResource);
        }
    }
}
