﻿namespace Admin2021.Backend.Permissions
{
    public static class BackendPermissions
    {
        public const string GroupName = "Backend";

        //Add your own permission names. Example:
        //public const string MyPermission1 = GroupName + ".MyPermission1";
    }
}