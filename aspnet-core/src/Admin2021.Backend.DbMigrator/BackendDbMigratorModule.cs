﻿using Admin2021.Backend.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace Admin2021.Backend.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(BackendEntityFrameworkCoreDbMigrationsModule),
        typeof(BackendApplicationContractsModule)
        )]
    public class BackendDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
